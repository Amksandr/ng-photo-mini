import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  users = [
    {name: 'Userr 1'},
    {name: 'Userr 2'},
    {name: 'Userr 3'},
    {name: 'Userr 4'},
    {name: 'Userr 5'},
    {name: 'Userr 6'},
  ];
}
